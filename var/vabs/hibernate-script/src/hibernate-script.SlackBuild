#!/usr/bin/bash
# This script assumes it will be launched within "/NAME/VERSION/src" dir.
# With all sources in "src" Your Vector Linux .tlz package, slack-desc,
# and slack-required will be found in "VERSION" dir. The extraction and
# build will be in a temp dir created in "NAME" dir, and then removed on exit.
# Comment out second to last line to keep this dir intact.
# Written by Sergey Portnov <sergius256 at narod dot ru>


NAME="hibernate-script"            #Enter package Name!
VERSION=${VERSION:-"2.0"}      #Enter package Version!
VL_PACKAGER=${VL_PACKAGER:-"Uelsk8s"}   #Enter your Name!
LINK=${LINK:-"http://tuxonice.net/files/hibernate-script-$VERSION.tar.gz"}  #Enter URL for package here!


#SYSTEM VARIABLES
#----------------------------------------------------------------------------
BUILDNUM=${BUILDNUM:-"1"}
VL_VERSION=${VL_VERSION:-"$(ls /var/log/packages/|grep vlconfig2|cut -d "-" -f4|cut -c 2-5)"}
BUILD=${BUILD:-"$BUILDNUM""$VL_VERSION"}
ARCH=${ARCH:-"noarch"}
CONFIG_OPTIONS=${CONFIG_OPTIONS:-""}
LDFLAG_OPTIONS=${LDFLAG_OPTIONS:-""}
#----------------------------------------------------------------------------


#SETUP PACKAGING ENVIRONMENT
#--------------------------------------------
CWD=$(pwd)
cd ../
RELEASEDIR=$(pwd)
cd $CWD
mkdir -p $RELEASEDIR/tmp
TMP=$RELEASEDIR/tmp
PKG=$TMP/package-$NAME
#--------------------------------------------



if [ $UID != 0 ]; then
   echo "You are not authorized to run this script. Please login as root"
   exit 1
fi

if [ ! -x /usr/bin/requiredbuilder ]; then
   echo "Requiredbuilder not installed, or not executable."
   exit 1
fi

if [ $VL_PACKAGER = "YOURNAME" ]; then
   echo 'Who are you?
   Please edit VL_PACKAGER=${VL_PACKAGER:-YOURNAME} in this script.
   Change the word "YOURNAME" to your VectorLinux packager name.
   You may also export VL_PACKAGER, or call this script with
   VL_PACKAGER="YOUR NAME HERE"'
   exit 1
fi


#CFLAGS SETUP
#--------------------------------------------
if [[ "$ARCH" = i?86 ]]; then
  ARCH=i586
  SLKCFLAGS="-O2 -march=i586 -mtune=i686"
  CONFIGURE_TRIPLET="i586-vector-linux"
elif [ "$ARCH" = "x86_64" ]; then
  SLKCFLAGS="-O2 -fpic"
  CONFIGURE_TRIPLET="x86_64-vlocity-linux"
fi

export CFLAGS="$SLKCFLAGS $CFLAG_OPTIONS"
export CXXFLAGS=$CFLAGS
export LDFLAGS="$LDFLAGS $LDFLAG_OPTIONS"
#--------------------------------------------


#GET THE SOURCE
#--------------------------------------------
if [ ! -f $CWD/$NAME-$VERSION.tar.* ]
then
        wget $LINK
fi
#--------------------------------------------


rm -rf $PKG
mkdir -p $PKG
cd $TMP
rm -rf $NAME-$VERSION


#EXTRACT SOURCES
#-----------------------------------------------------
echo "Extracting source..."
tar xvf $CWD/$NAME-$VERSION.tar.* || exit 1
#-----------------------------------------------------


cd $TMP/$NAME-$VERSION

# Put any Patches here
#-----------------------------------------------------
#patch -p1 <
#-----------------------------------------------------


#SET PERMISSIONS
#-----------------------------------------
echo "Setting permissions..."
chown -R root:root .
find . -perm 664 -exec chmod 644 {} \;
find . -perm 777 -exec chmod 755 {} \;
find . -perm 2777 -exec chmod 755 {} \;
find . -perm 775 -exec chmod 755 {} \;
find . -perm 2755 -exec chmod 755 {} \;
find . -perm 774 -exec chmod 644 {} \;
find . -perm 666 -exec chmod 644 {} \;
find . -perm 600 -exec chmod 644 {} \;
find . -perm 444 -exec chmod 644 {} \;
find . -perm 400 -exec chmod 644 {} \;
find . -perm 440 -exec chmod 644 {} \;
find . -perm 511 -exec chmod 755 {} \;
find . -perm 711 -exec chmod 755 {} \;
find . -perm 555 -exec chmod 755 {} \;
#-----------------------------------------



#CONFIGURE & MAKE
#----------------------------------------------------------------------
# If you are building a KDE-related app, then change the following
# arguments in the script below:
# --prefix=$(kde-config -prefix) \
# --sysconfdir=/etc/kde \
#
# Making these changes will ensure that your package will build in the
# correct path and that it will work seamlessly within the KDE environment.
#
#-----------------------------------------------------------------------

echo "Configuring source..."

PREFIX=$PKG/usr \
CONFIG_PREFIX=$PKG \
LOGROTATE_DIR=$PKG/etc/logrotate.d \
./install.sh


#######################################################################
#Miscellenious tweaks and things outside a normal ./configure go here #
#######################################################################

#mkdir -p $PKG/usr/share/applications
#cp $CWD/$NAME.desktop $PKG/usr/share/applications/$NAME.desktop
#mkdir -p $PKG/usr/share/pixmaps
#cp $CWD/$NAME.png $PKG/usr/share/pixmaps/$NAME.png

mkdir -p $PKG/usr/doc/$NAME-$VERSION
cp -a AUTHORS COPYING ChangeLog Credits MANIFEST README TODO \
$PKG/usr/doc/$NAME-$VERSION
cat $CWD/$NAME.SlackBuild > $PKG/usr/doc/$NAME-$VERSION/$NAME.SlackBuild

#----------------------------------------------------------------------

if [ -d $PKG/usr/share/man ] ; then
mkdir -p $PKG/usr/man
mv $PKG/usr/share/man/* $PKG/usr/man
rm -rf $PKG/usr/share/man
fi
find $PKG/usr/man -type f -exec gzip -9 {} \;

if [ -d $PKG/usr/share/info ] ; then
mkdir -p $PKG/usr/info
mv $PKG/usr/share/info/* $PKG/usr/info
rm -rf $PKG/usr/share/info
fi 
find $PKG/usr/info -type f -exec gzip -9 {} \;

mkdir -p $PKG/install


#if there is a slack-desc in src dir use it
if test -f $CWD/slack-desc; then
cp $CWD/slack-desc $RELEASEDIR/slack-desc
else
# This creates the white space in front of "handy-ruler" in slack-desc below.

LENGTH=$(expr length "$NAME")
SPACES=0
SHIM=""
until [ "$SPACES" = "$LENGTH" ]; do
SHIM="$SHIM "
let SPACES=$SPACES+1
done

# Fill in the package summary between the () below.
# Then package the description, License, Author and Website.
# There may be no more then 11 $NAME: lines in a valid slack-desc.

cat > $RELEASEDIR/slack-desc << EOF
# HOW TO EDIT THIS FILE:
# The "handy ruler" below makes it easier to edit a package description.  Line
# up the first '|' above the ':' following the base package name, and the '|'
# on the right side marks the last column you can put a character in.  You must
# make exactly 11 lines for the formatting to be correct.  It's also
# customary to leave one space after the ':'.

$SHIM|-----handy-ruler------------------------------------------------------|
$NAME: $NAME (frontend to TuxOnIce hibernation engine)
$NAME:
$NAME: hibernate-script package contains a set of shell scripts to provide
$NAME: TuxOnIce hibernation engine functionality. It does all the work to 
$NAME: prepare the computer for hibernation. Scriptlet API allows to perform 
$NAME: some actions like unloading modules or shutting applications. This
$NAME: package is required to run TuxOnIce-patched kernel.
$NAME: License: GPL
$NAME: Authors: 
$NAME: Website: http://www.tuxonice.net

EOF
fi
cat >> $RELEASEDIR/slack-desc << EOF



#----------------------------------------
BUILDDATE: $(date)
PACKAGER:  $VL_PACKAGER
HOST:      $(uname -srm)
DISTRO:    $(cat /etc/vector-version)
CFLAGS:    $CFLAGS
LDFLAGS:   $LDFLAGS
CONFIGURE: $(awk "/\.\/configure\ /" $TMP/$NAME-$VERSION/config.log)

EOF

cat $RELEASEDIR/slack-desc > $PKG/install/slack-desc
cat $CWD/doinst.sh > $PKG/install/doinst.sh

#STRIPPING
#------------------------------------------------------------------------------------------------------------------
cd $PKG
echo " "
echo "Stripping...."
echo " "
find . | xargs file | grep "executable" | grep ELF | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null
find . | xargs file | grep "shared object" | grep ELF | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null
#------------------------------------------------------------------------------------------------------------------
cd $PKG/etc/hibernate
for i in * ; do
    if [ -f $i ]; then
	echo "Renaming $i to $i.new"
	mv "$i" "$i.new"
    fi
done
cd $PKG
ls -C1 etc/hibernate/* | awk {print\(\"config\ \"\$1\)} - >> install/doinst.sh

#FINISH PACKAGE
#--------------------------------------------------------------
echo "Finding dependencies..."
requiredbuilder -v -y -s $RELEASEDIR $PKG
echo "Creating package $NAME-$VERSION-$ARCH-$BUILD.txz"
makepkg -l y -c n $RELEASEDIR/$NAME-$VERSION-$ARCH-$BUILD.txz

cd $CWD
echo "Cleaning up temp files..." && rm -rf $TMP
echo "Package Complete"
#--------------------------------------------------------------

# vim: set tabstop=4 shiftwidth=4 foldmethod=marker : ##
