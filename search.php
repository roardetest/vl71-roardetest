<?php
$loc_list = array(
	'vabs' => 'search-vabs.sh',
	'abs' => 'search-abs.sh',
	'sb' => 'search-sb.sh' );


echo "
<html>
<head>
	<title>Search package buildscripts.</title>
</head>
<body> \n
";

$end="\n
</body>
</html>
";

if( ! isset( $_REQUEST['pkg'] ) ) {
	echo "<p>Usage: <br> search.php?pkg=PKGNAME <br> or <br> search.php?pkg=PKGNAME&loc=LOCATION</p> \n";
	echo "<p>Valid options for LOCATION include \"vabs\", \"abs\", and \"sb\".</p> \n";
	echo $end;
	exit;
} else {
	$pkg = escapeshellarg( $_REQUEST['pkg'] );
}

if( ! isset( $_REQUEST['loc'] ) ) {
	$loc = 'vabs';
} else {
	if( ! array_key_exists( $_REQUEST['loc'], $loc_list ) ) {
		echo '<p>Specified location unknown. Using default location.</p>';
	} else {
		$loc = $_REQUEST['loc'];
#		$loc = escapeshellarg( $_REQUEST['loc'] );
#		Don't need to escape, since matched to whitelist?
	}
}

echo "<p>Using search params
<ul>
	<li>Package Name: $pkg</li>
	<li>Location: $loc</li>
	<li>Backend: $loc_list[$loc]</li>
</ul>
</p>";

exec( "./$loc_list[$loc] $pkg", $backend_results);
#exec( "./search-abs.sh bash", $backend_results);

echo "<p>Found results \n <ul>";
foreach( $backend_results as $find ) {
	echo "\t<li><a href=\"$find\">$find</a></li>\n";
}
echo "</ul> \n </p>";

echo $end;
?>
